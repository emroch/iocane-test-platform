"""
Crypto is not implemented yet. When it is, the high level crypto functions (that call a low level library) will be located here.
"""

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa


def hashToString(hash):
    return hash

def signData(data):
    pass

def genRSAKeyFile(filePath):
    key = rsa.generate_private_key(public_exponent=65537, key_size=2048, backend=default_backend())
    with open(filePath, "wb") as f:
        f.write(key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()))



# genRSAKeyFile("test.key")

#create a certificate request


#sign certificate request


#load
