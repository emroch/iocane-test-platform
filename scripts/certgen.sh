#!/usr/bin/env bash

# Automatically generates pikey and picert when ./certgen.sh "ip_address" is called

#openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 \
#       -subj "/C=US/ST=MA/L=Cambridge/O=Draper/CN=$1" \
#       -keyout ../certs/keys/pikey.pem -out ../certs/certs/picert.pem
#!/bin/bash

#Required
domain=$1
CN=$domain

#Change to your company details
C=US
ST=MA
L=Cambridge
O=Draper
OU=Systems
email=swansond.97@gmail.com

#Optional
PW=password

if [ -z "$domain" ]
then
    echo "Argument not present."
    echo "Useage $0 [common name]"

    exit 99
fi

echo "Generating key request for $domain"

#Generate a key
openssl genrsa -des3 -passout pass:$PW -out ../certs/keys/pikey.pem 2048 -noout

#Remove passphrase from the key. Comment the line out to keep the passphrase
echo "Removing passphrase from key"
openssl rsa -in ../certs/keys/pikey.pem -passin pass:$PW -out ../certs/keys/pikey.pem

#Create the request
echo "Creating CSR"
openssl req -new -key ../certs/keys/pikey.pem -out ../certs/pi.csr -passin pass:$PW \
    -subj "/C=$C/ST=$ST/L=$L/O=$O/OU=$OU/CN=$CN/emailAddress=$email"

#Ask the CA for a signed certificate
openssl x509 -req -in ../certs/pi.csr -CA ../certs/certs/cacert.pem -CAkey ../certs/keys/cakey.pem -CAcreateserial -out ../certs/certs/picert.pem


echo "---------------------------"
echo "-----Below is your CSR-----"
echo "---------------------------"
echo
cat ../certs/pi.csr

echo
echo "---------------------------"
echo "-----Below is your Key-----"
echo "---------------------------"
echo
cat ../certs/keys/pikey.pem


echo
echo "----------------------------"
echo "----Below is your picert----"
echo "----------------------------"
echo
cat ../certs/certs/picert.pem
