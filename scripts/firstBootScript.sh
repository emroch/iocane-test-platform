#!/usr/bin/env bash

#clone repo
git clone https://gitlab.com/nettijoe96/raspberry-pi-architecture.git ~/raspberry-pi-architecture

# install dependencies
sudo apt-get -y install python3
sudo apt-get -y install python3-pip
#sudo apt-get -y install docker.io

# install necessary python packages
pip3 install cryptography
pip3 install pyopenssl
