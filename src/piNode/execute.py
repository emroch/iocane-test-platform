"""
This file contains the functions for executing commands. The raspberry pi uses this file to execute commands. If the commands
are within the simulation, the raspberry pi calls functions in the bridge.py file, which is the file that is the bridge between
the simulation/program running on the pi's and the pi's themselves.
"""

import argparse
import os

import bridge
from constants import commands, ports, IP
from files import files, paths
import pi
import utility
import subprocess


"""
This will be used in the future to make it easier to coordinate/time sending files and commands. No need to risk sending a command without a file being there yet
This is especially important with more than 1 server/peer
"""
class CommandQueue:

    def __init__(self):
        self.queue = []
        self.size = 0
    def enqueue(self, ele):
        self.queue.append(ele)
        self.size += 1
    def dequeue(self):
        if self.size > 0:
            self.size -= 1
            ele = self.top()
            self.queue = self.queue[1:]
            return ele
        else:
            return False
    def top(self):
        if self.size > 0:
            return self.queue[0]

"""
Checks if a command can or cannot be run.
"""
def checkConditionsOfCommandFulfilled(command, commandType): #TODO later
    #for START we check if program is already running

    #for STOP we check if program is running

    #for DELETE we check if program even exists

    #for docker run commands we need to make sure the image is present
    return True

"""
Execute the command. In this function conditions are assumed to be met.
"""
def executeCommand(myPi, cmd):
    print('----------------')
    print('Executing Command:')
    print(cmd)
    #there are two types of commands: terminal/os and non-terminal/non-os.
    # More simply, commands that can be run on a terminal and not parsed by our function

    if cmd.type == commands.TERMINAL:
        #run on terminal!!
        try:
            os.system(cmd.command)
        except OSError:
            return None, None, OSError
    elif cmd.type == commands.INIT:  # creates appropriate simulation directory
        initSimulation(myPi, cmd.command)
    elif cmd.type == commands.REMOVE:
        cmd_str = cmd.command.split(" ")
        try:
            os.system("rm ~/raspberry-pi-architecture/src/" + cmd_str[1])
        except OSError:
            print("Simulation data file does not exist")
            pass
    elif cmd.type == commands.DOCKER:         # cmd: docker run HOST_IP
        cmdList = cmd.command.split(" ")
        try:
            os.system("sudo python3 " + utility.getSourcePath() + "/" + "insecureregistry.py --IP " + cmdList[2])
            os.system("sudo service docker restart")
        except FileNotFoundError:
            pass

        # print("pulling from registry\n")
        image_name = cmdList[2] + ":5000/" + cmdList[1]
        image_exists = utility.checkImageExists(image_name)
        if not image_exists:
            os.system("docker pull " + cmdList[2] + ":5000/" + cmdList[1])
        print("docker pull successful\n")
    elif cmd.type == commands.START:
        # format of commandStr: <simulation id/name> <path optional>
        start(myPi, cmd.command)
    elif cmd.type == commands.STOP:
        stop(myPi, cmd.command)
    elif cmd.type == commands.DELETE: #do this one last because this can easily be done with a terminal command
        delete(cmd.command)
    #all of these go directly to simulation unaltered
    elif utility.isAppSpecific(cmd.command):
        bridge.pipeToProcess(myPi, cmd.command)

    return cmd, None


"""
Initialize a simulation
"""
def initSimulation(myPi, cmdStr):
    cmdList = cmdStr.split(' ')
    simName = cmdList[1]
    # parse = argparse.ArgumentParser()
    # parse.add_argument('type')
    # parse.add_argument('simName')
    # parse.add_argument('type', nargs='?')
    # args = parse.parse_args(cmdList)
    # simName = args.simName

    myPi.initSimulation(simName)

    # # make docker network if docker images are used.
    # os.system("docker network rm " + simName) #remove old network called simName if one exists. TODO maybe only call this after callin docker network ls to see if one exists. For now this works
    # os.system("docker network create " + simName)
    # exists = os.path.exists(simName)
    # if exists:
    #     os.system("rm -rf " + simName)# TODO find a better way to deal with conflicts
    #     print("Removed existing directory:", simName)
    # print("simName:", simName)

    # simDir = utility.getSimulationPath(simName)
    # os.makedirs(simDir)
    # simProc = pi.SimulationProcess(simName)
    # myPi.initSimulation(simProc)
    #if it uses docker images, create a network called simName:


"""
Start a program/process. Track process IDs for termination purposes when stop is called
"""
def start(myPi, cmdStr):            # cmdStr returns "start + simName + Host_IP + type"
    cmdList = cmdStr.split(' ')

    parse = argparse.ArgumentParser()
    parse.add_argument('command')
    parse.add_argument('name')
    parse.add_argument('IP')
    parse.add_argument('type')
    parse.add_argument('path', nargs='?')
    args = parse.parse_args(cmdList)
    # print("Starting simulation ", args.simName)
    # print("Args.type:", args.type)
    status = myPi.startSimulation(args.name, args.type, args.path)
    if status is True:
        print(args.name, 'started successfully')

    # simName = args.name
    # path = args.path
    # if not args.command == commands.START:
    #     raise IOError
    # elif path == None:
    #     path = utility.getSimulationPath(simName)
    #
    # # TODO: this is almost certainly broken
    # #stop(cmdStr) #make sure another instance is not already running and if there is stop it
    # os.chdir(path)
    # open("pid.txt", "wb")
    # os.system("sh " + files.simulationRunScript + " > pid.txt")
    # pidLst = []
    # for pid in open("pid.txt", "rb"):
    #     pidLst += [pid]
    # os.chdir(utility.getRepoPath())
    #
    # simProc = myPi.getSimulation(simName)
    # myPi.startSimulation(simProc)
    # print(simName, 'started')
    # return pidLst

"""
Stop a program/process.
"killing" a program through the terminal is too forced, which is why this function will be updated soon with a call to the program which tell it to terminate
"""
def stop(myPi, cmd):
    cmdList = cmd.split(" ")
    simName = cmdList[1]

    myPi.stopSimulation(simName)

    # simProc = myPi.getSimulatioin(simName)
    # for pid in simProc.pids:
    #     os.system("kill -9 " + pid)
    # myPi.stopSimulation(simProc)

"""
Delete/uninstall a program
"""
def delete(command):
    pass



"""
openc2 query command. This query command is by no means complete because querying is simulation-dependent
"""
def query(myPi, command):
    cmdLst = command.split(" ")
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", nargs=1, type=str) #the argument after query is the simName
    parser.add_argument("-s", nargs="*") #elements after option are the specific item being queried
    parser.add_argument("-g", nargs="*") #elements after option are the general item being queried
    parser.add_argument("-a", nargs="*") #only -a is all peers. if specific IPs after -a then those specific peers.
    parser.add_argument("-w", nargs="*") #don't include personal data
    args = parser.parse_args(cmdLst)

    peers = False
    self = True
    simName = ""
    simulation = None
    queryPeers = []
    if "name" in args:
        simName = args.name[0]
        sim= myPi.getSimulation(simName)
    else:
        raise IOError
    if "a" in args:
        peers = True
        if len(args.a) == 0:
            #get all peers and put them in the list
            peers = bridge.getPeerList(myPi, sim)
        else:
            #make sure peers exist and then put them in the list
            peerList = args.a.split(",")
            actualPeers = bridge.getPeerList(myPi, sim)
            for peer in peerList:
                if peer in actualPeers:
                    queryPeers += [peer]
    if "w" in args:
        self = False
    generalItem = []
    if "g" in args:
        generalItem = args.g
    specificItem = []
    if "s" in args:
        specificItem = args.s
    if (generalItem == [] and specificItem == []) or (generalItem != [] and specificItem != []):
        raise IOError



    if not myPi.isRunning(simName):
        return False #TODO returns custom error. I need to customize error handling for all the errors in the program sometime
    data = ""
    if "g" in args:
        dataFile = generalQuery(myPi, sim, peers, self, generalItem)
    elif "s" in args:
        dataFile = specificQuery(myPi, sim, peers, self, specificItem)

def generalQuery(myPi, sim, peers, self, item):
    parser = argparse.ArgumentParser()

    #any more options go below here
    parser.add_argument("-t", nargs="+", type=str) #time range

    queryType = item[0]
    dataFile = ""
    if queryType == RANKING:
        #checkPermissions(sigList, commandType) #TODO might want to a check permissions that checks if whoever requested this query command has the permission to do so. This would be done for every command type and every query subcommand (since query is essentially an extension commands).
        dataFile = rankingQuery(myPi, sim, peers, self, item)
    #TODO add all general queries

    return dataFile

def rankingQuery(myPi, simName, peers, self, item):
    pass


def specificQuery(myPi, simName, peers, self, item):
    #the app-specific query function is inside a docker container. This function has to communicate with the docker container. Query response flows: container -> thisProgram -> orchestratorServer
    pass


"""
As commands get more complex and depend on conditions before they can be run, the processCommands function will be a roundrobin scheduler.
An example of a command that has a condition is "start program1" if program 1 doesn't exist yet because the command came before the
program files, then we have to put the command in a queue. The queue is processed by dequeueing a command, checking if the command
can be run (conditions met), running if it can be run, and if it cannot be run the command is enqueued again.
"""
# def processCommands(queue):
#
#     while True:
#         commandObj = queue.dequeue()
#         if commandObj != False:
#             if commandObj.type == SHUTDOWN:
#                 #TODO add a way to save the commands still in the queue in a database or file. Do this outside of this function though (when it returns)
#                 return
#             else:
#                 executeCommand(commandObj)
#         else:
#             #queue is empty; wait
#             time.sleep(3)
