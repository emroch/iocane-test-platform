import ssl
import socket
import utility

def createServerSideSocket(port, backlog=5, secure=True):
    '''Create a socket and SSLContext for server-side use.

    The server is the device that opens a socket and waits for something
    else to connect.  This function creates a standard Python socket that
    is bound to the local address on the specified port.  The optional
    backlog parameter specifies how many pending connections to allow before
    rejecting new ones.

    NOTE: The socket created by this function is not encrypted, and must be
    wrapped with the returned SSLContext to provide secure communication.

    @param port                 Integer port number to bind to
    @param backlog (optional)   Number of pending connections to allow. Defaults to 5
    @param secure (optional)    Allows for creating unsecured or secure sockets. Defaults to ssl sockets
    @returns A socket bound to `port`
    @returns SSLContext suitable for wrapping the socket. Returns None if socket isn't secure
    '''

    if secure == False:
        context = None
    else:
        context = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
        # TODO: password is passed automatically. Remove in the future
        context.load_cert_chain(certfile=utility.getPiCertPath(), keyfile=utility.getPiKeyPath(), password='password')

        #flag added so CRLs are checked during communications
        #VERIFY_CRL_CHECK_LEFT only checks against the root CA; also can use VERIFY_CRL_CHECK_CHAIN which checks all intermediates as well
        #context.verify_flags = ssl.VERIFY_CRL_CHECK_LEAF
        #using the path to the crl file to authenticate connections instead of just the ca
        #context.load_verify_locations(cafile = utility.getCRLPath())

    sock = socket.socket()
    sock.bind(('', port))
    sock.listen(backlog)

    return sock, context

def createClientSideSocket(server_ip, secure=True):
    '''Create an SSL-wrapped socket for client-side use.

    The client is the device that temporarily connects to the server to request or send data.  This function creates an SSLSocket that can be
    connected to the address of the desired server.

    @param server_ip The hostname (IP address) of the server to connect to
    @param secure (optional)    Allows for creating unsecured or secure sockets. Defaults to ssl sockets
    @returns a socket. Defaults to SSLSocket
    '''
    if secure == False:
        return socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    else:
        context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=utility.getCaCertPath())

        # flag added so CRLs are checked during communications
        # VERIFY_CRL_CHECK_LEFT only checks against the root CA; also can use VERIFY_CRL_CHECK_CHAIN which checks all intermediates as well
        #context.verify_flags = ssl.VERIFY_CRL_CHECK_LEAF
        # using the path to the crl file to authenticate connections instead of just the ca
        #context.load_verify_locations(cafile=utility.getCRLPath())

        conn = context.wrap_socket(socket.socket(), server_hostname=server_ip)
        return conn

def acceptSocket(sock, context, func, args, secure=True):
    '''Create a server side connection to an existing socket using an SSLContext.

    This function will spin until a client calls connect on the IP address of this
    device with the correct port.  Accepting the connection creates a new socket object
    which is then wrapped by the provided SSLContext. We then call the function `func`
    by passing the SSLSocket object followed by any other arguments required by the function.  When the function returns (or exits with error) the socket is closed.

    @param sock     Plain Python socket that is bound and listening on (IP, port)
    @param context  SSLContext specifying certificate and key files.
                    Purpose should be Purpose.CLIENT_AUTH
    @param func     Function to call in order to process incoming data
    @param args     Additional arguments to func
    @param secure (optional)    Allows for accepting unsecured or secure sockets. Defaults to ssl sockets
    '''
    # ensure that the provided parameters are the correct types
    assert isinstance(sock, socket.socket)
    if secure:
        assert isinstance(context, ssl.SSLContext)

    while True:
        # wait for an incoming connection, create a new socket, and wrap it
        # using the provided context if secure parameter is True
        newSocket, fromAddr = sock.accept()
        if secure:
            connStream = context.wrap_socket(newSocket, server_side=True)
        else:
            connStream = newSocket
        # call the provided function, closing the socket when the function exits
        try:
            func(connStream, *args)
        finally:
            try:
                connStream.shutdown(socket.SHUT_RDWR)
            except OSError as e:
                pass
            connStream.close()
