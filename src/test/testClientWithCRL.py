"""
I comment too
"""

import ssl
import socket
import utility
import keyServer.ca as ca
import keyClient
import OpenSSL
from cryptography.hazmat.primitives import serialization
import cryptography.x509.extensions as extensions



def createClientSideSocket(server_ip):
    '''Create an SSL-wrapped socket for client-side use.

    The client is the device that temporarily connects to the server to request or send data.  This function creates an SSLSocket that can be
    connected to the address of the desired server.

    @param server_ip The hostname (IP address) of the server to connect to
    @returns SSLSocket
    '''
    context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=utility.getCaCertPath())
    context.verify_flags = ssl.VERIFY_CRL_CHECK_LEAF
    context.load_verify_locations(utility.getRepoPath() + "/certs/crl/crl.pem")
    conn = context.wrap_socket(socket.socket(), server_hostname=server_ip)
    return conn
#
def createCRL():

    with open(utility.getCaKeyPath(), "rb") as f: key = f.read()
    with open(utility.getCaCertPath(), "rb") as f: cert = f.read()

    cacert = utility.loadCert_Cryptography(cert)
    cakey = utility.loadPrivKeyCryptography(key)
    c = ca.CA(cakey, cacert)
    builder = c.genCRL()
    revokeObj = keyClient.generateRevocation(utility.getRepoPath() + "/certs/certs/revokeCert.pem", extensions.ReasonFlags.unspecified)
    builder = c.addToCRL(revokeObj, builder)
    crl = c.finalizeCRL(builder)
    return crl

# crl = createCRL()
# with open("crl.pem", "wb") as f: f.write(crl.public_bytes(serialization.Encoding.PEM))

conn = createClientSideSocket("192.168.1.5")
conn.connect(("192.168.1.5", 5000))
conn.sendall(str.encode("test data"))
