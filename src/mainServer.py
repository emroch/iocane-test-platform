from __future__ import print_function, unicode_literals
from cryptography import x509
from cryptography.hazmat.primitives import serialization
from cryptography.x509.oid import NameOID   #used for getting IP from csr
import datetime
import time
import threading

from constants import commands, ports
from files import *
from files import files, paths
import keyServer.ca as ca
import os
import pickle
import sendUpgrade
import socketUtil
import utility


"""
Checks if a change has been made to the crl and then sends it out to peers
"""
def crlManager(caObj):
    while True:
        time.sleep(2)
        if caObj.crlChanges > 0:
            caObj.crlLock.acquire()
            print("a change was detected")
            try:
                IPList = utility.readIPList(utility.getKeyServerPeerList())
                pem = caObj.crl.public_bytes(serialization.Encoding.PEM)
                sendUpgrade.sendAll(IPList, str.encode(commands.CRL_DATA) + pem, commands.KEY_SERVER_REQUEST, ports.KEY_SERVER_PORT)
                with open(utility.getCRLPath(), 'w') as f: f.write(bytes.decode(pem))
                caObj.crlChanges = 0
            finally:
                caObj.crlLock.release()


"""
save the dictionary of certificates to a file
"""
def saveCertSet(certSet):
    pickle.dump(certSet, open(utility.getCertSetPath(), "wb"))


"""
load the dictionary of certificates from the file
"""
def loadCertSet():
    certSet = pickle.load(utility.getCertSetPath())
    return certSet


def getIPFromCSR(csr):
    """
    returns the common name (IP) value from csr
    :param csr: csr object from cryptography library
    :return: IP string
    """
    return csr.subject.get_attributes_for_oid(NameOID.COMMON_NAME)[0].value


def unencryptedKeyServerRequest(conn, caObj):
    """
    Unencrypted request processor. The only request that is allowed SIGN_REQUEST for an IP without a valid certificate.
    A list of taken IP addresses is stored using pickle in a python set in the KEY_SERVER_CERT_SET file
    :param conn: unsecured socket
    :param caObj: ca object that contains the ca keys and cert already loaded
    :return: signed cert or error
    """
    certSet = {}
    if os.path.exists(utility.getCertSetPath()):
        certSet = loadCertSet()

    data = b""
    while True:
        d = conn.recv(ports.BUFFER_SIZE)
        if d == commands.DATA_END or d == None: break
        else:
            data += d

    request = bytes.decode(data)[0]

    data = bytes.decode(data)[1:]
    data = str.encode(data)


    if request == commands.SIGN_REQUEST:
        csr = utility.loadCSR_Cryptography(data)
        IP = getIPFromCSR(csr)

        if IP in certSet:
            return Exception("valid cert already created for IP: " + IP)
        else:
            cert = caObj.createCert(csr)
    else:
        return Exception("invalid request for unencrypted channel")

    print("before sending certdata")
    certdata = cert.public_bytes(serialization.Encoding.PEM)
    conn.sendall(certdata)

    return cert



"""
process key server request
"""
def keyServerRequest(conn, caObj):
    r = conn.recv(1)
    request = bytes.decode(r)

    data = ""

    while True:
        d = conn.recv(ports.BUFFER_SIZE)
        if d == commands.DATA_END: break
        else:
            data += bytes.decode(d)


    # sign csr
    if request == commands.SIGN_REQUEST:
        csr = data
        cert = caObj.createCert(csr)
        #todo send cert back to key

        return cert
        # renewal
    elif request == commands.RENEWAL_REQUEST:
            cert = data
            caObj.renewal(cert)
        # revoke
    elif request == commands.REVOKE_REQUEST:
            revokedCert = data #Can this object be created w/o the privkey of the CA?
            #check if CRL is generated
            if caObj.crlBuilder == None:
                caObj.createNewCRL()
            revokedCert = parseRevokedCert(revokedCert)
            caObj.addToCRL(revokedCert) #todo getter and setter for crlBuilder?
            crl = caObj.finalizeCRL()
            caObj.setCRL(crl)  #todo in the future keep past crls for reference. This can be done inside setCRL()
            caObj.crlChanges += 1

def parseRevokedCert(data):
    ele = data.split(";")
    serial = int(ele[0])
    date = ele[1]
    d = datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
    reasonValue = ele[2]

    # get the ReasonFlags enum item by looking up the value
    # ('reason' will be of type x509.ReasonFlags)
    reason = x509.ReasonFlags(reasonValue)

    if reason == None:
        raise IOError
    else:
        revokedCert = utility.buildRevokedCert(serial, d, reason)
        return revokedCert


"""
Runs the key server: listen for and process signing. revocation, renewal requests, send updated CRLs
"""
def main():

    #make ca object
    with open(utility.getCaKeyPath(), "rb") as f: key = f.read()
    with open(utility.getCaCertPath(), "rb") as f: cert = f.read()
    cert = utility.loadCert_Cryptography(cert)
    key = utility.loadPrivKey_Cryptography(key)
    caObj = ca.CA(key, cert)

    #spawn a listen thread that listens to revocations, signing requests, and renewals
    maxConnections = len(utility.readIPList(utility.getKeyServerPeerList()))

    # create a socket to listen for data on KeyServer port
    keyServerSocket, context = socketUtil.createServerSideSocket(ports.KEY_SERVER_PORT)
    # create thread for the keyServerSocket to accept new connections and process requests for the key server
    keyServerThread = threading.Thread(name='KeyServer_Secure', target=socketUtil.acceptSocket,
                                       args=(keyServerSocket, context, keyServerRequest, (caObj,)))

    # create an unsecure socket
    unsecureSock, context = socketUtil.createServerSideSocket(ports.KEY_SERVER_UNENCRYPTED, secure=False)
    # create thread for the unencryptedKeyServerSocket to accept new connections and process SIGN_REQUESTS for the keyServer
    keyServerUnsecureThread = threading.Thread(name='KeyServer_Unsecure', target=socketUtil.acceptSocket,
                                               args=(unsecureSock, context, unencryptedKeyServerRequest, (caObj,), False))

    keyServerThread.start()
    keyServerUnsecureThread.start()

    # #spawn a CRL thread which sends updated CRL to peers
    # crlThread = threading.Thread(target=crlManager, args=(caObj,))
    # crlThread.start()

    # Sanity check to verify the threads launched successfully
    print('----------------')
    print('Running threads:')
    for thread in threading.enumerate():
        print(thread)
    print('----------------')


if __name__ == '__main__':
    main()
