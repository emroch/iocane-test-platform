"""
Contains network and data classes. Network classes stores network state information. Data objects are used when pis respond to openc2 commands.
"""

import pickle


appListFile = "appListFile.txt"
TIMEOUT_SECONDS = 10
savedNetworks = 10
snapshotPoint = 100

"""
The data class contains data send from a pi, the pi, the openC2command that led to this data as a response, and the simulation name.
"""
class Data:

    def __init__(self, simName, data, pi, command=None):
        '''

        :param simName: name of application that created the data
        :param data:    data
        :param pi:      pi that created the data
        :param command: the openC2 command that initiated the data
        '''
        self.simName = simName
        self.data = data
        self.pi = pi
        self.openC2Command = command

    def setOpenC2Command(self, command):
        self.openC2Command = command


"""
The group of pis associated with a network over time.
Every time there is a change, we make a new network object and set the old one as prevNetwork
Simulations link to the most recent network.
"""
class Network:
    
    def __init__(self, prevNetwork, piList):
        self.piList = piList
        #previous network state. instance becomes becomes child (previous network state of parent) when a node leaves or joins network.
        self.prevNetwork = prevNetwork
        self.nextNetwork = None

    # """
    # saves the last x states to file using pickle. The network objects are stored from oldest to newest.
    # The first entry contains all the information. The next n states only contain changes from the first entry.
    # """
    # def saveToFile(self, filename):
    #     net = self
    #     while net.prevNetwork != None:
    #         net = net.prevNetwork
    #
    #     #now we have the oldest network
    #     while net.nextNetwork != None:
    #         with open(filename, 'wb') as f:
    #             pickle.dump(net, f, pickle.HIGHEST_PROTOCOL)



"""
loads the network for a max number of state changes (any further is stored and can be grabbed at request)
"""
def loadNetwork(filename):
    error = ""
    with open(filename, 'rb') as f:
         while True:
             try:
                pickle.load(f, fix_imports=True, encoding='UTF-8', errors=error)
             except: break

    f.close()
