import os
import pickle
import time

from constants import commands, ports
import files
from files import files, paths
import orchestrator.network as network
import sendUpgrade
import utility
from pi import *


###
# Simulation class and functions
###
class Simulation:

    def __init__(self, simName, type, currNetwork, IPList, IPToSoftware, softwareToIP, startingTime, cmdLineLocation=None, keyServerIP=None):
        # set instance variables from constructor parameters
        self.simName = simName
        self.type = type
        self.currNetwork = currNetwork
        self.IPList = IPList
        self.IPToSoftware = IPToSoftware    # dictionary mapping list of software (values) to IP addresses (keys)
        self.softwareToIP = softwareToIP    # dictionary mapping list of IP addresses (values) to software (keys)
        self.startingTime = startingTime
        self.cmdLineLocation = cmdLineLocation
        self.keyServerIP = keyServerIP

        # for each IP in the dictionary, add the .metadata file to the list of software
        for IP in IPToSoftware:
            IPToSoftware[IP] += ["save,"+files.simulationMetadata]
        # set the list of IPs for the 'save,.metadata' entry
        self.softwareToIP["save,"+files.simulationMetadata] = IPList
        self.softwareList = ["save,"+files.simulationMetadata] + softwareToIP["order"]

    @classmethod
    def load(cls, simName):
        '''
        Create a Simulation object from existing data. If a pickled object file exists,
        load the instance state from it and return.  If no pickled file exists, create
        a new Simulation object based on the preset file.

        This is a class method because it does not depend on the state of an existing
        Simulation object.  It is a factory method (specialized constructor) to make
        creating Simulation objects easier.

        :param simName: name of the simulation to load
        :return: simulation object
        '''
        createNew = False

        objectFilePath = utility.getSimulationPath(simName) + '/' + files.simulationObjectFile
        # Attempt to read the object file. If it is not the
        # correct format or does not exist, set the flag
        try:
            with open(objectFilePath, 'rb') as f:
                line = f.readline()
                if line == b'PRESET' or os.stat(objectFilePath).st_size == 0:
                    createNew = True
        except FileNotFoundError:
            createNew = True

        # either load from a pickle file, or read the preset to create a new object
        if not createNew:
            with open(objectFilePath, 'rb') as f:
                return pickle.load(f, fix_imports=True, encoding='UTF-8')
        else:
            presetFilePath = utility.getSimulationPath(simName) + '/' + files.simulationPresetFile
            name, type, IPList, softwareToIP, IPToSoftware, bridgeLocation = loadPresetFile(presetFilePath)

            piList = [Pi(IP) for IP in IPList]
            currNetwork = network.Network(None, piList)
            if type == commands.DOCKER:
                container_exists = utility.checkContainerExists('silverwind/armhf-registry')  # TODO Don't hardcode registry image
                if container_exists is False:
                    create_registry()
                image_in_registry = utility.image_in_registry(name)
                if image_in_registry is False:
                    os.system("docker image tag " + name + " localhost:5000/" + name)
                    os.system("docker push localhost:5000/" + name)
            return cls(name, type, currNetwork, IPList, IPToSoftware, softwareToIP, time.asctime(), bridgeLocation)

    def save(self):
        objectFilePath = utility.getSimulationPath(self.simName) + '/' + files.simulationObjectFile
        with open(objectFilePath, 'wb') as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)


    #updgrade server. We use sendFile in sendUpgrade.py for this.
    def installSimulation(self):
        print('====================================')
        print('Installing Simulation:', self.simName)
        print('IPList:')
        print('   ', self.IPList)

        # set up the simulation directory on each pi
        # IPList = [pi.IP for pi in self.piList]
        cmd = 'init ' + self.simName + ' ' + commands.COMMAND_END
        sendUpgrade.sendAll(self.IPList, cmd, commands.EXECUTION_COMMAND, ports.COMMAND_PORT)

        # Docker simulations:
        if self.type == commands.DOCKER:
            presetFilePath = utility.getSimulationPath(self.simName) + '/' + files.simulationPresetFile
            image = image_name(presetFilePath)
            cmd = 'docker ' + image + ' ' + utility.getIP()
            sendUpgrade.sendAll(self.IPList, cmd, commands.EXECUTION_COMMAND, ports.COMMAND_PORT)

        # Other simulations
        for software in self.softwareList:
            if software.startswith(commands.DOCKER_IMAGE):
                continue
            # TODO: check if pi already has the specific software installed (by asking it directly or by keeping records)
            IPListInstall = self.softwareToIP[software]
            self.installSoftwareOnPis(IPListInstall, software)

        print('\nInstallation Finished')
        print('====================================')

    def runSimulation(self):
        # TODO: check if already running
        # send run script. The run script executes appropriate docker runs, program calls, etc.
        # print("Type from runSimulation:", self.type)
        cmd = commands.START + " " + self.simName + " " + utility.getIP() + " " + self.type
        # piList = self.piList
        # IPList = utility.getIPList(piList)
        sendUpgrade.sendAll(self.IPList, cmd, commands.EXECUTION_COMMAND, ports.COMMAND_PORT)

    def removeSimulation(self):
        cmd = commands.REMOVE + ' ' + "pi.data"
        sendUpgrade.sendAll(self.IPList, cmd, commands.EXECUTION_COMMAND, ports.COMMAND_PORT)

    def installSoftwareOnPis(self, IPlist, software):
        softLst = software.split(',')
        fileType = ''
        filename = softLst[1]
        filepath = self.simName + '/' + filename

        # TODO: Is there some reason to save the filetype?
        # if softLst[0] == commands.DOCKER_IMAGE:
        #     fileType = commands.DOCKER_IMAGE
        # elif softLst[0] in [commands.SAVE, commands.RUN]:
        #     fileType = utility.determineFileType(filename)

        cmd = '[' + commands.UPDATE + ' ' + self.simName + ' ' + softLst[0] + ']'

        # construct header
        header = sendUpgrade.buildFileHeader(filepath, time.asctime(), command=cmd)

        # call send file TODO: add an optional command in the header structure of a file so that there is no timing issues between running sh file and having the file on disk
        #if fileType == commands.DOCKER_IMAGE:
            #sendUpgrade.sendAll(IPlist, header, commands.EXECUTION_COMMAND, ports.COMMAND_PORT)

        sendUpgrade.sendAll(IPlist, header, commands.FILE_COMMAND, ports.FILE_PORT)


### Replaced by class method Simulation.load()
# def createSimulationObj(simName):
#     presetFilePath = utility.getSimulationPath(simName) + "/" + files.simulationPresetFile
#     name, IPList, softwareToIP, IPToSoftware, bridgeLocation = loadPresetFile(presetFilePath)
#     piList = []
#     for IP in IPList:
#         pi = Pi(IP)
#         piList += [pi]
#         # IPToPeer[IP] = [pi] + IPToPeer[IP]
#     # for IP in pis:
#     #     peerList = []
#     #     for peer in IPToPeer[IP][1:]:
#     #         if peer in IPToPeer:
#     #             peerList += IPToPeer[peer][0]
#     #     if IP in IPToSoftware:
#     #         nodeSoftware = IPToSoftware[IP]
#     #         currentState = PiState(nodeSoftware, IP, None, time.asctime())
#     #         IPToPeer[IP][0].setCurrentState(currentState)
#     currNetwork = network.Network(None, piList)
#     simulation = Simulation(name, currNetwork, piList, IPToSoftware, softwareToIP, time.asctime(), bridgeLocation)
#
#     return simulation

### Replaced by class method Simulation.load()
# def loadSimulation(simName):
#     noObject = False
#     objectFilePath = utility.getSimulationPath(simName) + "/" + files.simulationObjectFile
#     if os.path.isfile(objectFilePath):
#         with open(objectFilePath, 'rb') as f:
#             line = f.readline()
#             if line == "PRESET" or os.stat(files.simulationObjectFile).st_size == 0:
#                 noObject = True
#     else:
#         noObject = True
#
#     if noObject:
#         simulation = createSimulationObj(simName)
#     else:
#         #old simulation
#         with open(files.simulationObjectFile, 'rb') as simFile:
#             simulation = pickle.load(simFile, fix_imports=True, encoding='UTF-8')
#     return simulation


def image_name(presetFilePath):              # gets image name from pretext file
    with open(presetFilePath, 'r') as f:
        for line in f:
            # ignore lines beginning with '#' or containing only whitespace
            line = line.strip("\n").strip(" ")
            if line == '' or line.startswith('#'):
                continue
            # separate each part of the line
            lineLst = line.split("=")
            if lineLst[0] == commands.DOCKER_IMAGE:
                image = lineLst[1]
                return image


def loadPresetFile(presetFilePath):
    name = ""
    type = ''
    pis = []
    softwareToIP = {} #ip addresses of pis are mapped to simulation scripts and images m
    IPToSoftware = {}
    softwareToIP["order"] = []
    bridgeLocation = ""
    keyServerIP = None
    with open(presetFilePath, 'r') as f:
        for line in f:
            # ignore lines beginning with '#' or containing only whitespace
            line = line.strip('\n').strip(' ')
            if line == '' or line.startswith('#'):
                continue

            # separate each part of the line
            lineLst = line.split("=")

            # simulation name
            if lineLst[0] == "name":
                name = lineLst[1]

            elif lineLst[0] == "pi":
                pis = lineLst[1].split(",")

            # bridge location
            elif lineLst[0] == commands.BRIDGELOCATION:
                bridgeLocation = lineLst[1]

            # ?
            elif lineLst[0] in [commands.SAVE, commands.RUN]:  # startup containers (optional)
                software = lineLst[0] + "," + lineLst[1]
                IPList = lineLst[2].split(",")
                softwareToIP[software] = IPList
                softwareToIP["order"] += [software]
                for IP in IPList:
                    if IP not in IPToSoftware:
                        IPToSoftware[IP] = []
                    IPToSoftware[IP] += [software]

            #
            if lineLst[0] == commands.DOCKER_IMAGE:
                software = lineLst[0] + "," + lineLst[1]
                IPList = lineLst[2].split(",")
                softwareToIP[software] = IPList
                softwareToIP["order"] += [software]
                for IP in IPList:
                    if IP not in IPToSoftware:
                        IPToSoftware[IP] = []
                    IPToSoftware[IP] += [software]
                type = commands.DOCKER

    # print(type)
    createMetadata(name, presetFilePath)

    return name, type, pis, softwareToIP, IPToSoftware, bridgeLocation


def create_registry():
    os.system("docker pull silverwind/armhf-registry")
    os.system("docker run -d -v /srv/registry/data:/data -p 5000:5000"
              " --name registry silverwind/armhf-registry")


def createPiSpecificSimPreset(pi, simulation):
    #the only pi specific things are peers and software
    mySoftware = ""
    if pi.IP in simulation.IPToSoftware:
        mySoftware = simulation.IPToSoftware[pi.IP]
    myPeers = simulation.IPToPeer[pi.IP]
    if len(myPeers) > 1:
        myPeers = myPeers[1:]
    else:
        myPeers = []
    softString = ""
    for software in mySoftware:
        softString += software + ";"
    softString = softString[:-1]
    peerStr = ""
    for IP in myPeers:
        peerStr += IP + ";"
    peerStr = peerStr[:-1]
    return softString + "\n" + peerStr

# TODO: finish this function
def createMetadata(simName, pathToPreset):
    pathToMetadata = utility.getSimulationPath(simName) + '/' + files.simulationMetadata
    with open(pathToMetadata, 'w') as m:
        with open(pathToPreset, 'r') as p:
            for line in p:
                ele = line.split("=")
                if len(ele) == 0: continue
                if ele[0] == "name":
                    m.write(line)
                elif ele[0] == "bridgeLocation":
                    m.write(line)
                # elif ele[1] == "image": #docker
                #     m.write(line) #TODO this is wrong. finish this.
