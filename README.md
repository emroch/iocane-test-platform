# Raspberry Pi Simulation Testbed

This repository is a flexible system for running simulations/programs on raspberry pis, tracking the network, and controlling the network of pis.
The simulations have the option of using docker containers (recommended).

This is written fully in python. Right now the repository is incomplete. Eventually a key server will be involved to manage the identities of the pis.

## Project Directory Structure
Below is a tree view of the repository, with brief comments about the purpose of each file/folder.

```bash
raspberry-pi-architecture/
├── certs/
│   ├── certs/                          # X.509 Certificates used for SSL communication
│   │   ├── cacert.pem                      # Certificate of the CA. SSL functions look here to verify client certs.
│   │   ├── picert.pem                      # Certificate of the Pi, signed by CA. Should be unique across devices.
│   │   └── revokeCert.pem                  # Testing. Dummy certificate to test revocation lists
│   │
│   ├── crl/                            # Certificate Revocation Lists for testing
│   │   ├── crlEmpty.pem                    # Testing empty revocation list.
│   │   └── crlRevoke.pem                   # Testing single revoked certificate.
│   │
│   ├── csr/                            # Certificate Signing Requests
│   │   └── pi.csr                          # Intermediate file generated during picert creation.
│   │
│   ├── keys/                           # Private keys used for certificate creation
│   │   ├── cakey.pem                       # CA private key.  Would not be present if using a remote CA.
│   │   ├── pikey.pem                       # Pi private key.  Should be unique.
│   │   └── revokeKey.pem                   # Dummy key for revoked certificate.
│   │
│   └── README.md                       # Instructions for generating new keys/certificates.
│
├── docker_images/                      # Directory for storing downloaded Docker Images. Not currently used.
│
├── documentation/                      # Documentation directory, work in progress.
│   ├── templates/                          # Various file templates.
│   ├── Simulation Install.md               # Program flow for installing a new simulation.
│   └── Simulation Start.md                 # Program flow for starting an installed simulation.
│
├── other/                              # Code graveyard.  Obsolete/out dated code that was kept for reference.
│
├── scripts/                            # Utility scripts
│   ├── certgen.sh                          # Create new private key and certificate signed by the CA (in the certs/ directory)
│   ├── firstBootScript.sh                  # Script to run on first boot. Downloads Git repo and python dependencies.
│   ├── mpi_download.sh                     # Script to download and install mpi4py, used for distributed computing simulations.
│   └── receiveScript.sh                    # Script to auto-run on nodes to receive instructions.  Out-dated and incorrect.
│
├── simulation/                         # Simulation container folders, used to store simulation metadata and source files.
│   ├── hello/                              # Example source code simulation.
│   └── ubuntu/                             # Example Docker simulation.
│
├── src/                                # Source code directory.  All executable python scripts live here.
│   ├── keyServer/                          # Classes and functions used exclusively on the keyServer (CA).
│   │   ├── peerlist.txt
│   │   └── ca.py
│   │
│   ├── orchestrator/                       # Classes and functions used exclusively on the orchestrator.
│   │   ├── network.py
│   │   └── simulation.py
│   │
│   ├── piNode/                             # Classes and functions used exclusively on the nodes.
│   │   ├── docker/
│   │   │   ├── appSpecificExecute.py
│   │   │   └── dockerListener.py
│   │   │
│   │   └── execute.py
│   │
│   ├── test/                               # Testing functions for certificate revocation.
│   │
│   ├── bridge.py                           # Originally for forwarding commands to the running simulation.  Unused and probably unnecessary.
│   ├── constants.py                        # List of constants, divided into three namespaces: commands, ports, and ip addresses.
│   ├── files.py                            # List of filename constants.
│   ├── insecureregistry.py                 # Stand alone utility for adding a Docker registry to the list of accepted addresses.  Allows http access to our local registry.
│   ├── keyClient.py                        # Functions for communicating with the key server, including requesting a certificate.
│   ├── mainNode.py                         # Entry point for typical nodes.  Processes incoming commands and files and begins execution of simulations.
│   ├── mainOrchestrator.py                 # Entry point for the orchestrator.  Parses the preset.txt of the specified simulation and initiates commands and file transfer to nodes.
│   ├── mainServer.py                       # Entry point for the key server.  Processes certificate requests and responds with a new certificate.
│   ├── pi.py                               # Classes for the state and operation of each node.  Pi class is what starts/stops/installs/removes simulations, PiState stores the internal state of the node.
│   ├── receive.py                          # Processes incoming data based on connected port.
│   ├── sendUpgrade.py                      # Handles the sending of data to the nodes, constructing packet headers and packaging data to be sent.
│   ├── socketUtil.py                       # Utility functions for creating SSL connections.
│   └── utility.py                          # General utility functions.
│
├── storage/                            # Intended to store the state of the simulations on the network.  Not used.
└── temp_dir/                           # Temporary storage for anything that needs it.
```

---

### Getting Started
In order to install and start a new simulation on raspberry pi's, here are the steps:

On raspberry pi's or the computers that you are installing the simulation on:
1. `git clone https://gitlab.com/nettijoe96/raspberry-pi-architecture.git`
2. `python3 src/mainNode.py`


On the orchestration server:
1. `git clone https://gitlab.com/nettijoe96/raspberry-pi-architecture.git`
2. make a clientPeerList.txt file (see documentation/templates/clientPeerList.txt for more info
3. mkdir simulation/mySimulationName
4. make a preset.txt file and put it in the simulation/mySimulationName directory (see documentation/templates/preset.txt for more info)
5. make a run.sh file and put it in the simulation/mySimulationName directory (see documentation/templates/run.sh for more info)
6. put all the source files into the simulation/mySimulationName directory (unless the whole program is a group of docker containers)
7. `python3 src/mainOrchestrator.py --install mySimulationName` (make sure receive is already running!)
8. `python3 src/mainOrchestrator.py --start mySimulationName`
