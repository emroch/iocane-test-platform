The orchestration server needs to store the network state changes for all nodes across the network
over time and across many simulations. A simulation should define a set of programs that will be running. There will be
one network state trail per instance of a simulation.

I could use an already made data base service or I can use pickle to track changes to objects


without a database: pickle option:
each simulation instance has it's own directory.
file structure is:

simulation type 1
    simulation instance 1
        data
    simulation instance 2
        data
simulation type 1
    simulation instance1
        data
    simulation instance 2
        data

data has this file structure:

data
    metadata: all software be utilized (even if not used by every node), number of pis, timestamp of when simulation started, type of simulation (bgp, drone, etc)
    networkStateForLastXStates   #is loaded upon startup of simulation
    archivedNetworkStates        #may be pruned


