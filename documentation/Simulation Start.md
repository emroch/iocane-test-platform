# Simulation Start Flow

## Orchestrator

1. mainOrchestrator.main() runs and enters the args.start conditional.

2. Simulation is loaded using argument from install option (Simulation.load())

    1. load() attempts to load a simulation object file from `<repo>/simulation/<simName>/simulation.obj`

        - "simulation.obj" is unpacked using pickle

    2. If no object file exists, a new one is created

        - loadPresetFile() reads the preset file from `<repo>/simulation/<simName>/preset.txt`

        - Preset information is used to create new Simulation object.

3. Simulation is started by calling simInstance.runSimulation()

    - Send the command `START <simName>` to each Pi in the simulation's piList via the command port.

4. Start the OpenC2 command line interface and spawn two threads - one for analysis and one for the CLI.

## Node

1. mainNode.main() creates several threads to listen for incoming files and commands.  These threads each run socketUtil.acceptSocket(), which calls receive.receive() when a connection is made.

2.  receive.receive() determines how to treat the socket based on the port it is connected on.  In this case, we are interested only in the command port.

    1. If connected on COMMAND_PORT, receive data until the queue is empty.

    2. Create an execution command (utility.ExecutionCommand()) which stores a command string and the execution type.

    3. Execute the command (execute.executeCommand()) by determining the type and calling the appropriate function.

        - If the command begins with START, begin the simulation by calling execute.start()

        - Parse the command to get the simulation name and an optional path.  If no path is given, it defaults to `<repo>/simulation/<simName>`.

        - Move into the simulation's directory and execute the run script, writing the output to pid.txt and build the PID list before moving back to the repo folder.

        - Get the simulation process from the Pi by calling Pi.getSimulation(), then mark the simulation as started using Pi.startSimulation()
