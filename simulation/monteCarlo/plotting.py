#Susan Ni
#7/20/18
#Plotting data for monte carlo method simulation

import matplotlib.pyplot as plt

import utility

#-----------------------------------

def graphCircle(x, y):
    fig, ax = plt.subplots()
    plt.axis([0, 1, 0, 1])

    plt.plot(x, y, 'co', markersize=.5)

    circle = plt.Circle((0.5, 0.5), 0.5, color='r', fill=False)
    ax.add_artist(circle)

    plt.show()

def graphCircleColors(xIn, yIn, xOut, yOut):
    fig, ax = plt.subplots()
    plt.axis([0, 1, 0, 1])

    plt.plot(xIn, yIn, 'go', markersize = 0.01)
    plt.plot(xOut, yOut, 'ro', markersize = 0.01)

    plt.show()

def plotData(x, y, title='', xlabel='', ylabel='',log=False,subplt=False,line=False):
    '''
    plot results
    :param x: list of x values
    :param y: list of y values
    :param title: plot title
    :param xlabel: x axis label
    :param ylabel: y axis label
    :param log: optional param to make it a log plot; default to not a log plot
    :param subplt: optional pyplot subplot param i.e. 221; default to False indicating not plotting a subplot
    :param line: optional param to plot points or lines; defaults to plotting points
    ***IF USING SUBPLOTS, MUST MANUALLY PLT.SHOW()***
    :return: no return
    '''
    if subplt:
        plt.subplot(subplt)

    if line:
        plt.plot(x,y,markerfacecolor=utility.colors[utility.count],linestyle='-')
    else:
        plt.plot(x,y,markerfacecolor=utility.colors[utility.count],markeredgecolor='None',marker='o',linestyle='None')

    plt.title(title)

    if log:
        plt.yscale('log')
        plt.title('log '+title)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    if not subplt:
        plt.show()

def plot4SubPlots(rng, error, time, titleMod='',line=False):
    '''
    plots the 4 subplots for (log) error vs 10^x and (log) time vs 10^x
    :param rng: x values (10^x exponent for number of sample points)
    :param error: y values (error data)
    :param time: y values (time data)
    :param titleMod: adding modifier to the title (i.e. adding ' -- average')
    :param line: optional param to plot points or lines; defaults to plotting points
    :return: no return
    '''
    plotData(rng,error,'error vs 10^x'+titleMod,'# of sampled points (10^x)','error',log=True,subplt=221,line=line)
    plotData(rng,error,'error vs 10^x'+titleMod,'# of sampled points (10^x)','error',subplt=222,line=line)
    plotData(rng,time,'time vs 10^x'+titleMod,'# of sampled points (10^x)','time (s)',log=True,subplt=223,line=line)
    plotData(rng,time,'time vs 10^x'+titleMod,'# of sampled points (10^x)','time (log(s))',subplt=224,line=line)

def plotTrials(rng, errors, times=None, approach='', avg=False, line=False):
    '''
    plot trial data
    :param rng: range of x values to plot over (10^x exponent for number of sample points)
    :param errors: array of error for y values
    :param times: optional param of array of times for y values
    :param approach: string for main plot title (processes, pools, threads, or simple)
    :param avg: whether to plot trials separately or plot the average of all trials; defaults to separate
    :param line: optional param to plot points or lines; defaults to plotting points
    :return: no return
    '''
    if avg:
        error = []
        time = []
        for a in range(len(rng)):
            error.append(utility.average([errors[b][a] for b in range(len(errors))]))
            time.append(utility.average([times[c][a] for c in range(len(times))]))
        plot4SubPlots(rng, error, time, titleMod=' -- average',line=line)
    else:
        for a in range(len(errors)):
            plot4SubPlots(rng, errors[a], times[a],line=line)
            utility.updateColor()
    plt.subplots_adjust(hspace=0.65, wspace=0.35)
    plt.suptitle(approach+' -- '+str(len(errors))+' trials')
    plt.show()