#Susan Ni
#8/2/18
#Reading/writing/parsing data from a file

'''
improvements TODO:
how to check if file already exists?
'''

def write(filename, points, time):
    '''
    writes time data to specified file *** should be an existing file ***
    format: <number of sampled points> <number of pis> <time>
    :param filename: name of file to write to
    :param points: number of sampled points
    :param time: time data to write to file
    :return: no return
    '''
    piCount = sum(1 for line in open('machinefile.txt') if not line or not line == '\n')

    with open(filename, 'a') as f:
        f.write(str(points) + ' ')
        f.write(str(piCount) + ' ')
        f.write(str(time) + '\n')

def parse(filename):
    '''
    takes in file in the write() format and returns python array containing data
    each line is a list within the array; each space-separated number is an element in a list
    :return: list of data
    '''
    data = []

    with open(filename, 'r') as f:
        for line in f:
            data.append(line.rstrip().split(' '))

    return data
