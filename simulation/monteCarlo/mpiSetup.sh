#to run: "./mpi_download.sh ip1 ip2"

# download mpi4py
wget https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-2.0.0.tar.gz

#unzip the file
sudo tar -zxf mpi4py-2.0.0.tar.gz

sudo apt install libopenmpi-dev


# go to the directory
cd mpi4py-2.0.0

# install python-dev package
sudo apt-get update --fix-missing
sudo aptitude install python-dev

# run the setup
#python setup.py build
env MPICC=/usr/bin/mpicc pip3 install mpi4py

# Test that MPI works on your device
#mpiexec -n 5 python demo/helloworld.py
cd ~/raspberry-pi-architecture/simulation
mpiexec -n 2 --host $1,$2 python3 first.py
