#Susan Ni
#7/20/18
#Helper functions

#-----------------------------------

#colors = ['r','g','b','c','m','y','k']
colors = ['#7e1e9c','#15b01a','#0343df','#ff81c0','#653700','#e50000',
             '#95d0fc','#029386','#f97306','#96f97b','#c20078','#ffff14',
             '#75bbfd','#929591','#89fe05','#bf77f6','#9a0eea','#033500',
             '#06c2ac','#c79fef','#00035b','#d1b26f','#00ffff','#13eac9',
             '#06470c','#ae7181','#35063e','#01ff07','#650021','#6e750e',
             '#ff796c','#e6daa6','#0504aa','#001146','#cea2fd','#000000',
             '#ff028d','#ad8150','#c7fdb5','#ffb07c','#677a04','#cb416b',
             '#8e82fe','#53fca1','#aaff32','#380282','#ceb301','#ffd1df']
count = 0

def average(lst):
    '''
    averages the values in a list
    :param lst: list of numbers to average
    :return: arithemetic mean
    '''
    return sum(lst) / max(len(lst), 1)


def updateColor():
    global count
    count += 1
    if count >= len(colors):
        count = 0

def convertRange(lo, hi, step):
    '''
    takes in a min, max, and step and returns a list for the range of numbers
    :param lo: min value
    :param hi: max value
    :param step: step size
    :return: list of the range of numbers
    '''
    ele = lo
    rng = []
    while ele <= hi:
        rng.append(ele)
        ele += step
    return rng


def printTime(time):
    '''
    converts time in seconds to time in hours/min/secs
    :param time: time in seconds
    :return: time in seconds
    '''
    hours = time // 3600
    mins = (time % 3600) // 60
    secs = time % 60

    print('time:', str(hours), 'hour(s),', str(mins), 'min(s),', str(secs), 'sec(s)')

    return time